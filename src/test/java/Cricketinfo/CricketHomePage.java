package Cricketinfo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CricketHomePage extends CricketInitializer{
	private WebElement popup = driver.findElement(By.id("wzrk-cancel"));
	private WebElement liveScores = driver.findElement(By.xpath("//*[@id='navbarSupportedContent']/ul[1]/li[1]/a"));

	public void clickOnElements() {
		popup.click();
		liveScores.click();
	}

}
