package Cricketinfo;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class CricketInitializer {
	static WebDriver driver;

	public void initDriver() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\hp\\Downloads\\chromedriver_win32(2)\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	public void navigateEspncricinfo() {
		driver.navigate().to("https://www.espncricinfo.com");
	}

	public void maximizeWindow() {
		driver.manage().window().maximize();
	}

	public void waitToLoad() {
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	}


}
